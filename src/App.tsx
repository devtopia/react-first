import React, { ReactElement, useEffect, useState } from "react";

import "./App.css";
import ColorfulMessage from "./components/ColorfulMessage";

function App(): ReactElement {
  console.log("Parent Component");
  const [num, setNum] = useState(0);
  const [faceShowFlag, setFaceShowFlag] = useState(false);

  const onClickCountUp = (): void => {
    setNum(num + 1);
  };
  const onClickSwitchShowFlag = (): void => {
    setFaceShowFlag(!faceShowFlag);
  };

  // numの値だけ関心があるので、関心事を分離
  useEffect(() => {
    if (num > 0) {
      if (num % 3 === 0) {
        faceShowFlag || setFaceShowFlag(true);
      } else {
        faceShowFlag && setFaceShowFlag(false);
      }
    }
  }, [num]);

  return (
    <>
      <h1 style={{ color: "red" }}>こんにちは！</h1>
      <ColorfulMessage color="blue">ご元気ですか？</ColorfulMessage>
      <ColorfulMessage color="pink">元気です。</ColorfulMessage>
      <button onClick={onClickCountUp}>カウントアップ！</button>
      <br />
      <button onClick={onClickSwitchShowFlag}>on/off</button>
      <p>{num}</p>
      {faceShowFlag && <p>(・Д・)</p>}
    </>
  );
}

export default App;
