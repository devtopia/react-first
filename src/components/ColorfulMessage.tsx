import React, { ReactElement, ReactNode } from "react";

interface colorProps {
  color: string;
  children?: ReactNode;
}
const ColorfulMessage = (props: colorProps): ReactElement => {
  console.log("Child Component");
  const { color, children } = props;
  const contentStyle = {
    color,
    fontSize: "18px"
  };

  return <p style={contentStyle}>{children}</p>;
};

export default ColorfulMessage;
