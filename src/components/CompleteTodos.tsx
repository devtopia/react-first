import React, { ReactElement } from "react";

interface CompleteTodosProps {
  todos: string[];
  onClickBack: (index: number) => void;
}

export const CompleteTodos = (props: CompleteTodosProps): ReactElement => {
  const { todos, onClickBack } = props;
  return (
    <div className="complete-area">
      <p className="title">完了のTODO</p>
      <ul>
        {todos.map((todo: string, index: number) => {
          return (
            <div className="list-row" key={todo}>
              <li>{todo}</li>
              <button onClick={() => onClickBack(index)}>戻す</button>
            </div>
          );
        })}
      </ul>
    </div>
  );
};
